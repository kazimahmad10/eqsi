//
//  EndTask.swift
//  EQSI
//
//  Created by Kazim Ahmad on 06/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import UIKit

class EndTask: UIViewController {

    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var startedLabel: UILabel!
    @IBOutlet weak var endedLabel: UILabel!
    @IBOutlet weak var endButton: UIButton!
    
    var task: Task!
    let date = Date()
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        endButton.layer.cornerRadius = endButton.frame.height/2
        taskName.text = task.name
        startedLabel.text = task.startedAt
        formatter.dateFormat = Constants.dateFormat
    }
    
    @IBAction func didEndTask(_ sender: UIButton) {
        let alert = UIAlertController.init(title: "End Work", message: "Are you sure you want to end the task ?", preferredStyle: .alert)
        let confirm = UIAlertAction.init(title: "Confirm", style: .destructive) { (act) in
            self.endedLabel.text = "Ended At: " + self.formatter.string(from: self.date)
            self.task.ref?.updateChildValues(["is_completed": true,
                                              "ended_at": self.formatter.string(from: self.date)], withCompletionBlock: { (err, ref) in
                                                print("updated completed")
                                                self.endButton.isEnabled = false
                                              })
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(confirm)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
