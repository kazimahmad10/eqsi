//
//  LoginView.swift
//  EQSI
//
//  Created by Kazim Ahmad on 06/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import UIKit
import Firebase

class LoginView: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.layer.cornerRadius = 5.0
    }
    @IBAction func didLogin(_ sender: UIButton) {
        if emailField.text!.isEmpty {
            presentAlert(message: "Email is empty")
        }else if passwordField.text!.isEmpty{
            presentAlert(message: "Password is empty")
        }else{
            Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
                if user != nil {
                    Constants.userEmail = self.emailField.text!
                    Constants.userUID = user!.user.uid
                    if self.emailField.text!.contains("admin") {
                        let clientView = self.storyboard?.instantiateViewController(identifier: "adminView")
                        clientView?.modalPresentationStyle = .fullScreen
                        clientView?.modalTransitionStyle = .flipHorizontal
                        self.present(clientView!, animated: true, completion: nil)
                    }else{
                        let clientView = self.storyboard?.instantiateViewController(identifier: "clientsView")
                        clientView?.modalPresentationStyle = .fullScreen
                        clientView?.modalTransitionStyle = .flipHorizontal
                        self.present(clientView!, animated: true, completion: nil)
                    }

                }else{
                    if let err = error {
                        self.presentAlert(message: err.localizedDescription)
                    }
                }
            }
        }
    }
    
    func presentAlert(message: String) {
        let alert = UIAlertController.init(title: "Error", message: message, preferredStyle: .alert)
        let dismiss = UIAlertAction.init(title: "Okay", style: .default, handler: nil)
        alert.addAction(dismiss)
        self.present(alert, animated: true, completion: nil)
    }
}
