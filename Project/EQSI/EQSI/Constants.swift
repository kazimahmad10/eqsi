//
//  Constants.swift
//  EQSI
//
//  Created by Kazim Ahmad on 06/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import Foundation
import Firebase

class Constants {
    static var userEmail : String = ""
    static var userUID: String = ""
    static var dateFormat: String = "dd-MM-YYYY HH:mm:ss"
}
