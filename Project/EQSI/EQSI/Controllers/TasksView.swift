//
//  TasksView.swift
//  EQSI
//
//  Created by Kazim Ahmad on 06/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import UIKit
import Firebase

class TasksView: UIViewController {

    @IBOutlet weak var taskTable: UITableView!
    
    var tasks : [Task] = []
    let ref = Database.database().reference(withPath: "Tasks")
    
    var clientId: Int = 0
    let nowDate = Date()
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        taskTable.delegate = self
        taskTable.dataSource = self
        self.title = "Tasks"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addNewTask))
        getTasks()
    }
    
    @objc func addNewTask() {
        print("add new task")
        let alert = UIAlertController(title: "New Task",
                                      message: "Add a new task",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Start Work", style: .default) { _ in
          guard let textField = alert.textFields?.first,
              let text = textField.text else { return }
            
            self.formatter.dateFormat = Constants.dateFormat
            let start = self.formatter.string(from: self.nowDate)
            
            let item = Task.init(name: text, client_id: self.clientId, started_at: start, ended_at: "", is_completed: false)
          
          let itemRef = self.ref.child(text.lowercased())
            itemRef.setValue(item?.toAnyObject())
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addTextField()
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func getTasks() {
        var newTasks : [Task] = []
        print(clientId)
        ref.queryOrdered(byChild: "client_id").queryEqual(toValue: clientId).observe(.value) { (snapshot) in
            self.tasks.removeAll()
            newTasks.removeAll()
            for client in snapshot.children {
                if let snap = client as? DataSnapshot {
                    let taskToAdd = Task.init(snapshot: snap)
                    newTasks.append(taskToAdd)
                }else{
                    print("not snapshot")
                }
            }
            self.tasks = newTasks
            self.taskTable.reloadData()
            
        }
    }
    
    
}
extension TasksView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tasks[indexPath.row].isCompleted {
            print("already completed")
        }else{
            let endview = self.storyboard?.instantiateViewController(identifier: "endTaskView") as! EndTask
            endview.task = tasks[indexPath.row]
            self.navigationController?.pushViewController(endview, animated: true)
        }
    }
}

extension TasksView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("TableCell", owner: self, options: [:])?.first as! TableCell
        cell.taskName.text = tasks[indexPath.row].name
        cell.completedLabel.text = tasks[indexPath.row].isCompleted ? "Completed" : "Not Completed"
        cell.completedLabel.textColor = tasks[indexPath.row].isCompleted ? UIColor.green : UIColor.red
        cell.sideImage.image = #imageLiteral(resourceName: "Task_Complete")
        return cell
    }
    
    
}
