//
//  AdminClientsView.swift
//  EQSI
//
//  Created by Kazim Ahmad on 06/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import UIKit
import Firebase

class AdminClientsView: UIViewController {

    @IBOutlet weak var collectionview: UICollectionView!
    var clients : [Client] = []
    
    let ref = Database.database().reference(withPath: "Clients")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionview.delegate = self
        collectionview.dataSource = self
        self.title = "Clients"
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical

        collectionview.setCollectionViewLayout(layout, animated: true)

        collectionview.register(UINib.init(nibName: "CollectionCell", bundle: nil), forCellWithReuseIdentifier: "collectionCell")
        getClients()
    }
    
    func getClients() {
        var newClients : [Client] = []
        
        ref.observe(.value) { (snapshot) in
            for client in snapshot.children {
                if let snap = client as? DataSnapshot {
                    let clientToAdd = Client.init(snapshot: snap)
                    newClients.append(clientToAdd)
                }else{
                    print("not snapshot")
                }
            }
            self.clients = newClients
            DispatchQueue.main.async {
                self.collectionview.reloadData()
            }
        }
    }

}


extension AdminClientsView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionview.frame.width, height: collectionview.frame.height/2)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let goToTask = self.storyboard?.instantiateViewController(identifier: "taskView") as! TasksView
        goToTask.clientId = clients[indexPath.item].id
        self.navigationController?.pushViewController(goToTask, animated: true)
    }
}

extension AdminClientsView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return clients.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! CollectionCell
        cell.nameLabel.text = clients[indexPath.item].name
        cell.emailLabel.text = clients[indexPath.item].email
        cell.clientImage.image = indexPath.item % 2 == 0 ? #imageLiteral(resourceName: "client2") : #imageLiteral(resourceName: "client")
        return cell
    }
    
}

