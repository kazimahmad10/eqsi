//
//  ViewController.swift
//  EQSI
//
//  Created by Kazim Ahmad on 05/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var timeLabel: UILabel!
    
    var clients : [Client] = []
    
    let ref = Database.database().reference(withPath: "Clients")
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)

        collectionview.delegate = self
        collectionview.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical

        collectionview.setCollectionViewLayout(layout, animated: true)

        collectionview.register(UINib.init(nibName: "CollectionCell", bundle: nil), forCellWithReuseIdentifier: "collectionCell")
        getClients()
    }
    @objc func tick() {
        timeLabel.text = DateFormatter.localizedString(from: Date(),
                                                              dateStyle: .none,
                                                              timeStyle: .medium)
    }
    func getClients() {
        var newClients : [Client] = []
        
        ref.observe(.value) { (snapshot) in
            for client in snapshot.children {
                if let snap = client as? DataSnapshot {
                    let workers = snap.childSnapshot(forPath: "workers_assigned")
                    for worker in workers.children {
                        if let workerSnap = worker as? DataSnapshot {
                            if let value = workerSnap.value as? [String: Any] {
                                if let id = value["id"] as? String {
                                    print(id)
                                    if id == Constants.userUID {
                                        let clientToAdd = Client.init(snapshot: snap)
                                        newClients.append(clientToAdd)

                                    }
                                }
                            }
                        }
                    }
                }else{
                    print("not snapshot")
                }
            }
            self.clients = newClients
            DispatchQueue.main.async {
                self.collectionview.reloadData()
            }
        }
    }
    
    @IBAction func didLogout(_ sender: UIBarButtonItem) {
        let sheet = UIAlertController.init(title: "Logout", message: "Are you sure you want to logout ?", preferredStyle: .actionSheet)
        let yes = UIAlertAction.init(title: "Logout", style: .destructive) { (act) in
            Constants.userEmail = ""
            Constants.userUID = ""
            self.dismiss(animated: true, completion: nil)
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        sheet.addAction(yes)
        sheet.addAction(cancel)
        self.present(sheet, animated: true, completion: nil)
    }
    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionview.frame.width, height: collectionview.frame.height/2)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let goToTask = self.storyboard?.instantiateViewController(identifier: "taskView") as! TasksView
        goToTask.clientId = clients[indexPath.item].id
        self.navigationController?.pushViewController(goToTask, animated: true)
    }
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return clients.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! CollectionCell
        cell.nameLabel.text = clients[indexPath.item].name
        cell.emailLabel.text = clients[indexPath.item].email
        cell.clientImage.image = indexPath.item % 2 == 0 ? #imageLiteral(resourceName: "client2") : #imageLiteral(resourceName: "client")
        return cell
    }
    
}

