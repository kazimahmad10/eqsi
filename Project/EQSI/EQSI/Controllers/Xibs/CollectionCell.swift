//
//  CollectionCell.swift
//  EQSI
//
//  Created by Kazim Ahmad on 05/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {

    @IBOutlet weak var clientImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 20.0
    }

}
