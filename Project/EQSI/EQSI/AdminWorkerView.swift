//
//  AdminWorkerView.swift
//  EQSI
//
//  Created by Kazim Ahmad on 06/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import UIKit
import Firebase

class AdminWorkerView: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    let ref = Database.database().reference(withPath: "Workers")
    var workers: [Worker] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        self.title = "Workers"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .reply, target: self, action: #selector(logout))

        // Do any additional setup after loading the view.
        getWorkers()
    }
    
    @objc func logout() {
        let sheet = UIAlertController.init(title: "Logout", message: "Are you sure you want to logout ?", preferredStyle: .actionSheet)
        let yes = UIAlertAction.init(title: "Logout", style: .destructive) { (act) in
            Constants.userEmail = ""
            Constants.userUID = ""
            self.dismiss(animated: true, completion: nil)
        }
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        sheet.addAction(yes)
        sheet.addAction(cancel)
        self.present(sheet, animated: true, completion: nil)
    }
    
    func getWorkers() {
        var newWorker : [Worker] = []
        ref.observe(.value) { (snapshot) in
            newWorker.removeAll()
            for client in snapshot.children {
                if let snap = client as? DataSnapshot {
                    let workerToAdd = Worker.init(snapshot: snap)
                    newWorker.append(workerToAdd)
                }else{
                    print("not snapshot")
                }
            }
            self.workers = newWorker
            self.tableview.reloadData()
        }
    }

}


extension AdminWorkerView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension AdminWorkerView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("TableCell", owner: self, options: [:])?.first as! TableCell
        cell.taskName.text = workers[indexPath.row].name
        cell.completedLabel.text = workers[indexPath.row].email
        cell.sideImage.image = #imageLiteral(resourceName: "images")
        return cell
    }
    
    
}
