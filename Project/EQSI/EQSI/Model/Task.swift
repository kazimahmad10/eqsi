//
//  Task.swift
//  EQSI
//
//  Created by Kazim Ahmad on 06/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import Foundation
import Firebase

struct Task {
    let ref: DatabaseReference?
    var key: String = ""
    var name: String = ""
    var clientID: Int = 0
    var startedAt: String = ""
    var endedAt: String = ""
    var isCompleted: Bool = false
    
    init(snapshot: DataSnapshot) {
        if let value = snapshot.value as? [String: AnyObject] {
            self.name = value["name"] as? String ?? ""
            self.clientID = value["client_id"] as? Int ?? 0
            self.startedAt = value["started_at"] as? String ?? ""
            self.endedAt = value["ended_at"] as? String ?? ""
            self.isCompleted = value["is_completed"] as? Bool ?? false
        }
        self.ref = snapshot.ref
        self.key = snapshot.key
    }
    
    init?(name: String, client_id: Int, started_at: String, ended_at: String, is_completed: Bool, key: String = "") {
        self.ref = nil
        self.key = key
        self.name = name
        self.clientID = client_id
        self.startedAt = started_at
        self.endedAt = ended_at
        self.isCompleted = is_completed
    }
    
    func toAnyObject() -> Any {
      return [
        "name": name,
        "client_id": clientID,
        "started_at": startedAt,
        "ended_at": endedAt,
        "is_completed": isCompleted
        
      ]
    }
}
