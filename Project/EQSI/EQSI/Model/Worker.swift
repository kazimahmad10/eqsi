//
//  Worker.swift
//  EQSI
//
//  Created by Kazim Ahmad on 06/01/2020.
//  Copyright © 2020 Kazim Ahmad. All rights reserved.
//

import Foundation
import Firebase

struct Worker {
         let ref: DatabaseReference?
        var key: String = ""
         var name: String = ""
        var id: Int = 0
        var email: String = ""
                 
         init(snapshot: DataSnapshot) {
            if let value = snapshot.value as? [String: AnyObject] {
                self.name = value["name"] as? String ?? ""
                self.id = value["id"] as? Int ?? 0
                self.email = value["email"] as? String ?? ""
            }
            self.ref = snapshot.ref
            self.key = snapshot.key
         }
}
